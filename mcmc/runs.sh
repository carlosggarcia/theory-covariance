#!/bin/bash
# Modify these three variables and add the chains that you want to
# launch/resume at the end of the script
queue=cmb
nodes=2
chains=4


######
# Automatic part
if [[ $queue == "cmb" ]]; then
    cores=24
elif  [[ $queue == "berg" ]]; then
    cores=28
elif  [[ $queue == "normal" ]]; then
    cores=1
else
    echo "queue not recognized. Choose cmb or berg"
    exit 1
fi

chains_per_node=$(($chains/$nodes))
threads=$(($cores/$chains_per_node))

if (( $chains % $nodes != 0 )); then
    echo "The number of chains or nodes is wrong"
    exit 1
elif (( $cores % $chains_per_node != 0 )); then
    echo "The number of chains per node ($chains_per_node) is not compatible with the number of cores ($cores) in the nodes"
    exit 1
fi


# Launcher
#########
create_launcher() {
    param=$1

    if [[ ! -d "./tmp" ]]; then
        mkdir ./tmp
    fi

    code=$(mktemp -p ./tmp)
    chmod +x $code
    cat <<EOF > $code
#!/bin/bash
export OMP_NUM_THREADS=$threads
export COBAYA_USE_FILE_LOCKING=False

/usr/local/shared/slurm/bin/srun -N $nodes -n $chains --ntasks-per-node $chains_per_node -m cyclic --mpi=pmi2 $HOME/mambaforge/envs/thcov/bin/python -m cobaya run $param
EOF
    echo $code

}

add_job_to_queue() {
    param=$1
    logname=$2
    launcher=$(create_launcher $param)
    addqueue -n ${nodes}x${cores} -s -q $queue -m 1  -c $name -o log/$logname.log $launcher
}

launch_chain() {
    name=$1
    logname=$name
    param=input/$name.yml
    add_job_to_queue $param $logname
}

resume_chain() {
    name=$1
    param=chains/$name/$name
    logname=${name}_resume
    add_job_to_queue $param $logname
}


# Chains
############

### LINEAR vs NONLINEAR
# Only 1 bin
# launch_chain desy3wl3_linearPk_lmax75_lmin20  # Linear scale cuts 
# launch_chain desy3wl3_linearPk_lmax2048_lmin20
# launch_chain desy3wl3_nonlinearPk_lmax2048_lmin20
# launch_chain desy3wl3_linearPk_lmax2048_lmin20_ThCovSmallRange
# launch_chain desy3wl3_linearPk_lmax2048_lmin20_ThCovCellCorrectedPriorsRange
# launch_chain desy3wl3_linearPk_lmax2048_lmin20_ThCovCellCorrectedBetterDPkPriorsRange
# launch_chain desy3wl3_linearPk_lmax2048_lmin20_ThCovCellCorrectedDPkDzPriorsRange
# launch_chain desy3wl3_linearPk_lmax2048_lmin20_ThCovPriorsRange
# launch_chain desy3wl3_linearPk_lmax2048_lmin20_ThCovSmallRange-15evk # Test whether this matters or 5 evk are enough for the small range
# launch_chain desy3wl3_linearPk_lmax2048_lmin20_ThCovSmallRange-200ksamples  # Test whether this matters or 20k samples are enough for the small range
#
# launch_chain desy3wl3_nonlinearPk_lmax2048_lmin20_SmallRange # with default emulator

# Mock data
# launch_chain desy3wl3_nonlinearPk_lmax2048_lmin20_mockdata
# launch_chain desy3wl3_linearPk_lmax2048_lmin20_mockdata
# launch_chain desy3wl3_linearPk_lmax2048_lmin20_ThCovCellCorrectedPriorsRange_mockdata
# launch_chain desy3wl3_linearPk_lmax2048_lmin20_ThCovCellCorrectedBetterDPkPriorsRange_mockdata
# launch_chain desy3wl3_linearPk_lmax2048_lmin20_ThCovCellCorrectedDPkDzPriorsRange_mockdata
# launch_chain desy3wl3_linearPk_lmax75_lmin20_mockdata  # Linear scale cuts 

# 
# All bins
# launch_chain desy3wl_linearPk_lmax2048_lmin20_ThCovCellCorrectedPriorsRange
# launch_chain desy3wl_linearPk_lmax2048_lmin20_ThCovCellCorrectedBetterDPkPriorsRange
# launch_chain desy3wl_linearPk_lmax2048_lmin20_ThCovCellCorrectedDPkDzPriorsRange
# launch_chain desy3wl_linearPk_lmax2048_lmin20_ThCovPriorsRange
# launch_chain desy3wl_linearPk_lmax2048_lmin20_ThCovSmallRange
# launch_chain desy3wl_linearPk_lmax2048_lmin20
# launch_chain desy3wl_linearPk_lmaxScaleCuts_lmin20

# Mock data
# launch_chain desy3wl_nonlinearPk_lmax2048_lmin20_mockdata
# launch_chain desy3wl_linearPk_lmax2048_lmin20_mockdata
# launch_chain desy3wl_linearPk_lmax2048_lmin20_ThCovCellCorrectedPriorsRange_mockdata
# launch_chain desy3wl_linearPk_lmax2048_lmin20_ThCovCellCorrectedBetterDPkPriorsRange_mockdata
launch_chain desy3wl_linearPk_lmax2048_lmin20_ThCovCellCorrectedDPkDzPriorsRange_mockdata
# launch_chain desy3wl_linearPk_lmaxScaleCuts_lmin20_mockdata

### HALOFIT vs BACCOEMU
# Only 1 bin
# launch_chain desy3wl3_HFITPk_lmax482_lmin20  # HALOFIT-BACCOEMU scale cuts
# launch_chain desy3wl3_HFITPk_lmax2048_lmin20
# launch_chain desy3wl3_HFITPk_lmax2048_lmin20_Cellcorrected_ThCovPriorsRange
#
# All bins
# launch_chain desy3wl_HFITPk_lmax2048_lmin20_ThCovPriorsRange
# launch_chain desy3wl_HFITPk_lmaxScaleCuts_lmin20
# launch_chain desy3wl_HFITPk_lmax2048_lmin20_Cellcorrected_ThCovPriorsRange

### BARYONS vs NO-BARYONS

### Emulator error



# vim: tw=0
