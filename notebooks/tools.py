"""
Functions to use across different notebooks
"""
import sacc
import pyccl as ccl
import numpy as np
from scipy.interpolate import interp1d
import os


def get_lhc_priors_range(k, samples=20000):
    # Compute the Pk with CCL for the whole range since it will be useful also for the HALOFIT vs BACCOEmu
    if samples==20000:
        suffix = ''
    else:
        suffix = f'-{samples}samples'
    fname = f'pk_ccl_lhc_PriorsRange{suffix}.npz'
    if os.path.isfile(fname):
        output = dict(np.load(fname))
    else:
        # Sample in a latinhypercube
        # Cosmological parameters are the ones in MCMC
        # plus the scale factor
        # Priors used in the MCMCs
        priors = {'A_sE9': [0.5, 5],
                  'Omega_m': [0.1, 0.7],
                  'Omega_b': [0.03, 0.07],
                  'h': [0.55, 0.91],
                  'n_s': [0.87, 1.07],
                  'm_nu': [0.0559, 0.400],
                  'expfactor': [0.275, 1]} # From the nonlinear Pk. a=0.275 -> z = 2.6, which is way higher than the N(z) of DESY3 galaxies.
        params = list(priors.keys())

        print('Computing the LHC for:', params)
        n = len(params)
        limits = np.array([priors[param] for param in params])
        spread = True
        seed = 3832  # This way it is deterministic
        # Use scipy because it's way faster
        method = 'scipy'
        H = latinhypercube(n, samples, limits, spread, seed, method)

        # Params dictionary
        params_lhc = {k: H[:, i] for i, k in enumerate(params)}
        params_lhc['w0'] = -np.ones(samples)
        params_lhc['wa'] = np.zeros(samples)

        pk_lin_ccl = np.zeros((samples, k.size))
        pk_nonlin_ccl = np.zeros((samples, k.size))

        skip_index = []
        for i in np.arange(samples):
            h = params_lhc['h'][i]
            pars_ccl = {'Omega_b': params_lhc['Omega_b'][i],
                        'Omega_c': params_lhc['Omega_m'][i] - params_lhc['Omega_b'][i] - params_lhc['m_nu'][i]/(93.14 * h**2),
                        'h': h,
                        'n_s': params_lhc['n_s'][i],
                        'A_s': params_lhc['A_sE9'][i]*1e-9,
                        'm_nu': params_lhc['m_nu'][i]}
            try:
                cosmo = ccl.Cosmology(**pars_ccl, transfer_function='boltzmann_camb', matter_power_spectrum='halofit')
                cosmo.compute_nonlin_power()
                pk_lin_ccl[i] = cosmo.get_linear_power()(k=k*h, a=params_lhc['expfactor'][i]) * h**3
                pk_nonlin_ccl[i] = cosmo.get_nonlin_power()(k=k*h, a=params_lhc['expfactor'][i]) * h**3
            except ValueError as e:
                print(f"Error in index {i}, with error {str(e)}")
                skip_index.append(i)

        output = dict(k_hMpc=k, pk_lin=pk_lin_ccl, pk_nonlin=pk_nonlin_ccl, skip_index=skip_index, params=params, lhc=H)
        np.savez_compressed(fname, output)

    return output


def get_params_inbounds_of_baccoemu(params, emulator, mpk_emulator):
    def get_within_bounds(cospar):
        within_bounds = []
        for i, parname in enumerate(mpk_emulator.emulator[emulator]['keys']):
            if parname not in cospar:
                continue
            val = cospar[parname]

            lower_bound = mpk_emulator.emulator[emulator]['bounds'][i][0]
            upper_bound = mpk_emulator.emulator[emulator]['bounds'][i][1]

            within_bounds.append((val >= lower_bound) * (val <= upper_bound))

        inbounds = np.all(np.array(within_bounds), axis=0)
        return inbounds

    def sigma8tot_2_sigma8cold(emupars, sigma8tot):
        """Use baccoemu to convert sigma8 total matter to sigma8 cdm+baryons
        """
        if hasattr(emupars['omega_cold'], '__len__'):
            _emupars = {}
            for pname in emupars:
                _emupars[pname] = emupars[pname][0]
        else:
            _emupars = emupars
        A_s_fid = 2.1e-9
        sigma8tot_fid = mpk_emulator.get_sigma8(cold=False,
                                       A_s=A_s_fid, **_emupars)
        A_s = (sigma8tot / sigma8tot_fid)**2 * A_s_fid
        return mpk_emulator.get_sigma8(cold=True, A_s=A_s, **_emupars)

    size = params['Omega_m'].size
    cospar = {
        'omega_cold': params['Omega_m'] - params['m_nu']/(93.14*params['h']**2),
        'omega_baryon': params['Omega_b'],
        'ns': params['n_s'],
        'hubble': params['h'],
        'neutrino_mass': params['m_nu'],
        'w0': params['w0'],
        'wa': params['wa'],
        'expfactor': params['expfactor']}

    within_bounds = get_within_bounds(cospar)

    newcospar = {}
    for p, v in cospar.items():
        newcospar[p] = v[within_bounds]

    newcospar['A_s'] = params['A_sE9'][within_bounds] * 1e-9
    newcospar['sigma8_cold'] = mpk_emulator.get_sigma8(**newcospar, cold=True)
    del newcospar['A_s']

    within_bounds_newcospar = get_within_bounds(newcospar)
    within_bounds[within_bounds] *= within_bounds_newcospar

    for p, v in newcospar.items():
        newcospar[p] = v[within_bounds_newcospar]

    return newcospar, within_bounds


def get_sacc_th_marg(s, evk, vk_x_data_std_pk2d, data_mean_pk2d, dz_shifts,
                     m_wl, vk_sigma):
    """
    Compute the covariance matrix from the theory error
    Args:
        s (sacc.Sacc)
        evk (array of eigenvalues)
        vk_x_data_std_pk2d (list of ccl.Pk2D)
        data_mean_pk2d (ccl.Pk2D)
        dz_shifts (dictionary)
        m_wl (dictionary)
        vk_sigma (float or array)

    Returns:
        sacc.Sacc: sacc file with the data and correction applied
        sacc.Sacc: sacc file with the correction (mean and thcov)
        dict: projected eigenvectors that go into thcov
    """
    snew = s.copy()

    # Concatenate eigenvectors and data_mean
    evk = np.concatenate([evk, [1]])
    vk_x_data_std_pk2d = np.concatenate([vk_x_data_std_pk2d, [data_mean_pk2d]])

    if isinstance(vk_sigma, float) or isinstance(vk_sigma, int):
        vk_sigma = np.ones(vk_x_data_std_pk2d.size -1) * vk_sigma
    vk_sigma = np.array(vk_sigma)

    # Initialize cosmology
    cosmo = ccl.CosmologyVanillaLCDM()

    # Instatiate the ccl tracers
    ccl_tracers = {}
    z = np.linspace(0, 4, 100)

    for trn, trv in snew.tracers.items():
        dndz = interp1d(trv.z, trv.nz, bounds_error=False, fill_value=0) # Interpolate the dndz
        ccl_tracers[trn] = ccl.WeakLensingTracer(cosmo, dndz=(z, dndz(z - dz_shifts[trn])), has_shear=True, ia_bias=None)


    # Project the eigenvectors
    print("Projecting eigenvectors", flush=True)
    wl = {}
    mean_correction = np.zeros_like(snew.mean)
    for trs in snew.get_tracer_combinations():
        print("Projecting for tracer combination {}".format(trs), flush=True)
        ix = snew.indices(tracers=trs)
        bpw = snew.get_bandpower_windows(ix)
        l_bpw = bpw.values
        w_bpw = bpw.weight.T
        wl_trs = []
        wl_trs_noevk = []
        for i in range(evk.size):
            print("Projecting eigenvector number = {}".format(i), flush=True)
            evki = evk[i]
            vki = vk_x_data_std_pk2d[i]
            cl = ccl.angular_cl(cosmo, ccl_tracers[trs[0]], ccl_tracers[trs[1]], l_bpw, p_of_k_a=vki, limber_integration_method="spline")
            cl_bin = (1 + m_wl[trs[0]]) * (1 + m_wl[trs[1]]) * w_bpw.dot(cl)
            wl_trs.append(evki * cl_bin)
            wl_trs_noevk.append(cl_bin)

        # Rescale the data vector to account for the theory error
        mean_correction[ix] = wl_trs[-1]

        # Save the computed projected eigenvectors without the mean
        wl[trs] = np.array(wl_trs[:-1])

    # Compute covariance matrix
    print("Computing covariance matrix", flush=True)
    cov = np.zeros_like(snew.covariance.covmat)
    for i, trs1 in enumerate(snew.get_tracer_combinations()):
        wl_trs1 = wl[trs1]
        ix1 = snew.indices(tracers=trs1)
        for trs2 in snew.get_tracer_combinations()[i:]:
            wl_trs2 = wl[trs2]
            ix2 = snew.indices(tracers=trs2)
            # Factor from eigenvalues evk
            factor_vk = vk_sigma**2
            covij = np.sum(factor_vk[:, None, None] * wl_trs1[:, :, None] * wl_trs2[:, None, :], axis=0)
            cov[np.ix_(ix1, ix2)] = covij
            cov[np.ix_(ix2, ix1)] = covij.T

    # Create the sacc file with the correction only
    scorr = snew.copy()
    scorr.mean = mean_correction
    scorr.add_covariance(cov, overwrite=True)

    # Create the sacc file with the data and correction
    snew.mean = snew.mean - mean_correction
    snew.add_covariance(s.covariance.covmat + cov, overwrite=True)

    return snew, scorr, wl


# Matteo's functions
def crossmatch(X1, X2, max_distance=np.inf, neighbour=0, boxsize=None):
    from scipy.spatial import cKDTree

    """Cross-match the values between X1 and X2

    By default, this uses a KD Tree for speed.

    Parameters
    ----------
    X1 : array_like
        first dataset, shape(N1, D)
    X2 : array_like
        second dataset, shape(N2, D)
    max_distance : float (optional)
        maximum radius of search.  If no point is within the given radius,
        then inf will be returned.
    boxsize : float (optional)
        if given, assumes a periodic box with sidelength boxsize
    Returns



    Returns
    -------
    dist, ind: ndarrays
        The distance and index of the closest point in X2 to each point in X1
        Both arrays are length N1.
        Locations with no match are indicated by
        dist[i] = inf, ind[i] = N2
    """
    X1 = np.asarray(X1, dtype=float)
    X2 = np.asarray(X2, dtype=float)

    N1, D = X1.shape
    N2, D2 = X2.shape

    if D != D2:
        raise ValueError('Arrays must have the same second dimension')

    if boxsize is None:
        kdt = cKDTree(X2)
    else:
        kdt = cKDTree(X2, boxsize=boxsize)

    dist, ind = kdt.query(X1, k=(1+neighbour), distance_upper_bound=max_distance)

    return dist, ind


def nearest_neighbour(X):
    """
    Returns the index and the distance to the nearest neighbour in a N-dimensional space
    """

    dist, ind = crossmatch(X, X, max_distance=np.inf, neighbour=1)
    return dist[:, 1], ind[:, 1]


def latinhypercube(n, samples, limits=None, spread=True, seed=None, method='bacco'):
    def _spread_lh(values, ndims, max_niter=10000):
        nrejected = 0
        total_dist = 0
        for x in range(ndims):
            for y in range(0, x):
                dist, ind = nearest_neighbour(values[:, [x, y]])
                total_dist += np.sum(dist)

        for i in range(max_niter):
            # We select two random points without replacement
            i1, i2 = np.random.choice(len(ind), 2, replace=False)

            # Let's swap values and accept it if it creates a more homogeneous distribution
            for dim in range(ndims):
                values[i1, dim], values[i2, dim] = values[i2, dim], values[i1, dim]
                new_total_dist = 0
                for x in range(ndims):
                    for y in range(0, x):
                        dist, ind = nearest_neighbour(values[:, [x, y]])
                        new_total_dist += np.sum(dist)

                if (new_total_dist < total_dist):
                    values[i1, dim], values[i2, dim] = values[i2, dim], values[i1, dim]
                    nrejected += 1
                    if nrejected == 2000:
                        return values
                else:
                    nrejected = 0
                    total_dist = new_total_dist

        return values

    if method == 'bacco':

        np.random.seed(seed)

        # Generate the intervals
        cut = np.linspace(0, 1, samples + 1)

        # Fill points uniformly in each interval
        u = np.random.rand(samples, n)
        a = cut[:samples]
        b = cut[1:samples + 1]
        rdpoints = np.zeros_like(u)
        for j in range(n):
            rdpoints[:, j] = u[:, j]*(b-a) + a

        # Make the random pairings
        H = np.zeros_like(rdpoints)
        for j in range(n):
            order = np.random.permutation(range(samples))
            H[:, j] = rdpoints[order, j]

        if spread is True:
            H = _spread_lh(H, n)

        if limits is not None:
            H = (H *(limits[:,1]-limits[:,0])+limits[:,0])

        return H

    elif method == 'scipy':
        #Warning, this method require latest version of scipy 1.11, results will be different for 1.1 and it will not work for lower versions
        #To keep the same format as the bacco method:
        # n       -> dimension of the LH
        # samples -> number of points of the LH

        from scipy.stats import qmc
        sampler = qmc.LatinHypercube(d=n, seed=seed)
        sample = sampler.random(n=samples)
        if limits is None:
            return sample
        else:
            return qmc.scale(sample, limits[:,0], limits[:,1])
